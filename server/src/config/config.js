const PORT = 4000
const SERVER = `http://localhost:${PORT}`
const DB_TEST = 'mongodb://localhost/cityawake'
module.exports = {
  PORT, SERVER, DB_TEST
}
const express = require('express')
const app = express()
const register = require('../api/routes/register')
const listar = require('../api/routes/listar')
const login = require('../api/routes/login')

module.exports = (server) => {
  const api = express.Router()
  server.use('/api', api)

  const DataBase = require('../api/db/dbService')
  DataBase.register(api, '/db')

  api.get('/register', register.registerGet)
  api.post('/register', register.registerPost)
  api.get('/listar', listar.listar)
  api.post('/login', login.loginPost)
}
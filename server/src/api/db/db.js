const restful = require('node-restful')
const mongoose = restful.mongoose

const vigilanteSchema = new mongoose.Schema({
  nome: {
    type: String,
    required: [true, 'Nome é obrigatório']
  },
  senha: {
    type: String,
    required: [true, 'Senha é obrigatório']
  },
  cpf: {
    type: String,
    required: [true, 'Cpf é obrigatório']
  },
  endereco: {
    type: String,
    required: [true, 'Endereço é obrigatório']
  },
  bairro: {
    type: String,
    required: [true, 'Bairro é obrigatório']
  },
  telefone: {
    type: String
  },
  email: {
    type: String,
    required: [true, 'Email é obrigatório']
  },
  porte: {
    type: Boolean,
  },
  disp_hora: {
    type: String
  }

})

const empresaSchema = new mongoose.Schema({
  nome: {
    type: String,
    required: [true, 'Nome é obrigatório']
  },
  senha: {
    type: String,
    required: [true, 'Senha é obrigatório']
  },
  cnpj: {
    type: String,
    required: [true, 'CNPJ/CPF é obrigatório']
  },
  endereco: {
    type: String
  },
  telefone: {
    type: String
  },
  assaltado: {
    type: Boolean
  },
  email: {
    type: String
  },
  bairro: {
    type: String
  },
  horario_func: {
    type: String
  }
})



module.exports = restful.model('cityawake', {vigilanteSchema, empresaSchema})
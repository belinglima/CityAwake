const db = require('../db/dbService')
const vig = require('../db/db')

module.exports = {
  registerGet(req, res, next){
    res.send('register page')
  },
  registerPost(req,res, next){ 
    const nome = req.body.nome
    const senha = req.body.senha
    const cpf = req.body.cpf
    const endereco = req.body.endereco
    const bairro = req.body.bairro
    const telefone = req.body.telefone
    const email = req.body.email
    const porte = req.body.porte
    const disp_hora = req.body.disp_hora
    db.findOne({cpf}).then(userFind => {
      if(userFind)
        res.status(400).send({erro:'Usuário já existe'})
      else{
        const novoVigilante = {
          nome, senha, cpf, endereco, bairro, telefone, email, porte, disp_hora
        }
        db.create(novoVigilante, (err) => {
          if(err){
            res.status(400).send({erro:'Erro ao adicionar no banco'})
          } else {
            res.json({ cpf })
          }
        })
      }
    })
  }
}
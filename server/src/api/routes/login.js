const db = require('../db/dbService')
const vig = require('../db/db')

module.exports = {
  loginPost(req, res, next) {
    const cpf = req.body.cpf
    const senha = req.body.senha

    db.findOne({cpf}).then(userFind => {
      if(senha === userFind.senha){
        res.send({'status':'logado'})
      } else {
        res.send({'status':'senha ou cpf inválidos'})
      }
    })
  }

}